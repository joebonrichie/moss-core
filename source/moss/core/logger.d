/*
 * This file is part of moss-core.
 *
 * Copyright © 2020-2022 Serpent OS Developers
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

// Mash up of https://github.com/repeatedly/colored-logger for the mosslog class and https://github.com/joakim-brannstrom/colorlog for the global register stuff
// Work in-progress

module moss.core.logger;

import logger = std.experimental.logger;
import std.stdio : File, stdout;
import std.concurrency : Tid;
import std.datetime : SysTime;

// FIXME: control verbosity level, current level is suited for tracing/debug
class mosslog : logger.FileLogger
{
    enum Color : string
    {
        Clear  = "\033c",
        Normal = "\033[0m",
        Black  = "\033[1;30m",
        Red    = "\033[1;31m",
        Green  = "\033[1;32m",
        Yellow = "\033[1;33m",
        Blue   = "\033[1;34m",
        Purple = "\033[1;35m",
        Cyan   = "\033[1;36m",
        White  = "\033[1;37m"
    }

  private:
    immutable string[logger.LogLevel] _colorMap;

  public:
    this(File file, const logger.LogLevel lv = logger.LogLevel.info) @safe
    {
        this(file, null, lv);
    }

    this(File file, in string[logger.LogLevel] colorMap, const logger.LogLevel lv = logger.LogLevel.info) @safe
    {
        super(file, lv);
        _colorMap = buildColorMap(colorMap);
    }

    override protected void beginLogMsg(string file, int line, string funcName,
                                        string prettyFuncName, string moduleName, logger.LogLevel logLevel,
                                        Tid threadId, SysTime timestamp, logger.Logger logger) @safe
    {

        this.file.lockingTextWriter().put(_colorMap[logLevel]);
        super.beginLogMsg(file, line, funcName, prettyFuncName, moduleName, logLevel, threadId, timestamp, logger);
    }

    override protected void finishLogMsg()
    {
        auto lt = this.file.lockingTextWriter();
        lt.put(cast(string)Color.Normal);
        lt.put('\n');
        this.file.flush();
    }

  private:
    immutable(string[logger.LogLevel]) buildColorMap(in string[logger.LogLevel] colorMap) @trusted pure
    {
        import std.exception : assumeUnique;
        import std.traits : EnumMembers;

        string[logger.LogLevel] result;

        foreach (ll; EnumMembers!(logger.LogLevel)) {
            if (ll in colorMap) {
                result[ll] = colorMap[ll];
                continue;
            }

            // Default color mapping
            final switch (ll) {
            case logger.LogLevel.all, logger.LogLevel.off:
                break;
            case logger.LogLevel.trace:
                result[ll] = Color.Blue;
                break;
            case logger.LogLevel.info:
                result[ll] = Color.Green;
                break;
            case logger.LogLevel.warning:
                result[ll] = Color.Yellow;
                break;
            case logger.LogLevel.error:
                result[ll] = Color.Purple;
                break;
            case logger.LogLevel.critical:
                result[ll] = Color.Red;
                break;
            case logger.LogLevel.fatal:
                result[ll] = Color.Cyan;
                break;
            }
        }

        return result.assumeUnique;
    }
}

// FIXME: Can all this shit be simplified?

///  Call this function to register the logger globally across a project
void registerLogger() @safe {
    //logger.globalLogLevel = toLogLevel(mode);

    logger.sharedLog = new mosslog(trustedStdout, logger.LogLevel.info);

    () @trusted { registered(logger.sharedLog, ""); }();
}

// FIXME: Ugly hack
@property File trustedStdout() @trusted
{
    return stdout;
}

/// Create a logger for the module and make it configurable from "outside" via the registry.
void make(LoggerT)(const logger.LogLevel lvl = logger.LogLevel.all, string name = __MODULE__) @trusted {
    registered(new LoggerT(lvl), name);
}

/// Register a logger for the module and make it configurable from "outside" via the registry.
void registered(logger.Logger logger, string name = __MODULE__) {
    synchronized (poolLock) {
        loggers[name] = cast(shared) logger;
    }
}

/** Log a mesage to the specified logger.
 *
 * This only takes the global lock one time and then cache the logger.
 */
logger.Logger log(string name = __MODULE__)() @trusted {
    static logger.Logger local;

    if (local !is null)
        return local;

    synchronized (poolLock) {
        if (auto v = name in loggers) {
            local = cast()*v;
            return cast()*v;
        }
    }
    // FIXME: not implemented - just use a generic error?
    throw new UnknownLogger("no such logger registered: " ~ name);
}

private:

import core.sync.mutex : Mutex;

// Mutex for the logger pool.
shared Mutex poolLock;
shared logger.Logger[string] loggers;

shared static this() {
    poolLock = cast(shared) new Mutex();
}
